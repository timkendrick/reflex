use reflex::core::{create_record, Expression, ExpressionFactory, HeapAllocator};
use reflex_stdlib::{CollectList, Effect, Get};

use crate::actor::{timeout::EFFECT_TYPE_TIMEOUT, timestamp::EFFECT_TYPE_TIMESTAMP};

pub fn import_time<T: Expression>(
    factory: &impl ExpressionFactory<T>,
    allocator: &impl HeapAllocator<T>,
) -> T
where
    T::Builtin: From<CollectList> + From<Effect> + From<Get>,
{
    create_record(
        [
            (
                factory.create_string_term(allocator.create_static_string("timeout")),
                factory.create_lambda_term(
                    2,
                    factory.create_application_term(
                        factory.create_builtin_term(Effect),
                        allocator.create_triple(
                            factory.create_string_term(
                                allocator.create_string(String::from(EFFECT_TYPE_TIMEOUT)),
                            ),
                            factory.create_application_term(
                                factory.create_builtin_term(CollectList),
                                allocator.create_unit_list(factory.create_variable_term(1)),
                            ),
                            factory.create_variable_term(0),
                        ),
                    ),
                ),
            ),
            (
                factory.create_string_term(allocator.create_static_string("timestamp")),
                factory.create_lambda_term(
                    1,
                    factory.create_application_term(
                        factory.create_builtin_term(Effect),
                        allocator.create_triple(
                            factory.create_string_term(
                                allocator.create_static_string(EFFECT_TYPE_TIMESTAMP),
                            ),
                            factory.create_application_term(
                                factory.create_builtin_term(CollectList),
                                allocator.create_unit_list(factory.create_application_term(
                                    factory.create_builtin_term(Get),
                                    allocator.create_pair(
                                        factory.create_variable_term(0),
                                        factory.create_string_term(
                                            allocator.create_static_string("interval"),
                                        ),
                                    ),
                                )),
                            ),
                            factory.create_nil_term(),
                        ),
                    ),
                ),
            ),
        ],
        factory,
        allocator,
    )
}
