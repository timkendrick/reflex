use std::time::{SystemTime, UNIX_EPOCH};

pub fn get_timestamp_millis(timestamp: SystemTime) -> u64 {
    timestamp
        .duration_since(UNIX_EPOCH)
        .unwrap_or_default()
        .as_millis()
        .min(u64::MAX as u128) as u64
}
