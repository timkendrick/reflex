use reflex::core::{
    Applicable, Expression, ExpressionFactory, HeapAllocator, Reducible, Rewritable,
};
use reflex_dispatcher::{Action, TaskFactory};
use reflex_interpreter::compiler::Compile;
use reflex_macros::{blanket_trait, task_factory_enum, Matcher};

use crate::{
    task::{
        bytecode_worker::{BytecodeWorkerAction, BytecodeWorkerTask, BytecodeWorkerTaskFactory},
        evaluate_handler::{
            EffectThrottleTaskFactory, EvaluateHandlerTask, EvaluateHandlerTaskAction,
        },
        wasm_worker::{WasmWorkerAction, WasmWorkerTask, WasmWorkerTaskFactory},
    },
    AsyncExpression, AsyncExpressionFactory, AsyncHeapAllocator,
};

pub mod bytecode_worker;
pub mod evaluate_handler;
pub mod wasm_worker;

blanket_trait!(
    pub trait RuntimeTaskAction<T: Expression>:
        BytecodeWorkerAction<T> + WasmWorkerAction<T> + EvaluateHandlerTaskAction
    {
    }
);

blanket_trait!(
    pub trait RuntimeTask<T, TFactory, TAllocator>:
        EvaluateHandlerTask
        + BytecodeWorkerTask<T, TFactory, TAllocator>
        + WasmWorkerTask<T, TFactory, TAllocator>
    where
        T: Expression,
        TFactory: ExpressionFactory<T>,
        TAllocator: HeapAllocator<T>,
    {
    }
);

// TODO: Implement Serialize/Deserialize traits for RuntimeTaskFactory
task_factory_enum!({
    #[derive(Matcher, Clone)]
    pub enum RuntimeTaskFactory<T, TFactory, TAllocator>
    where
        T: Expression,
        TFactory: ExpressionFactory<T>,
        TAllocator: HeapAllocator<T>,
    {
        BytecodeWorker(BytecodeWorkerTaskFactory<T, TFactory, TAllocator>),
        WasmWorker(WasmWorkerTaskFactory<T, TFactory, TAllocator>),
        EvaluateHandler(EffectThrottleTaskFactory),
    }
    impl<T, TFactory, TAllocator, TAction, TTask> TaskFactory<TAction, TTask>
        for RuntimeTaskFactory<T, TFactory, TAllocator>
    where
        T: AsyncExpression + Rewritable<T> + Reducible<T> + Applicable<T> + Compile<T>,
        TFactory: AsyncExpressionFactory<T> + Default,
        TAllocator: AsyncHeapAllocator<T> + Default,
        TAction: Action + RuntimeTaskAction<T> + EvaluateHandlerTaskAction + Send + 'static,
        TTask: TaskFactory<TAction, TTask>,
    {
    }
});
