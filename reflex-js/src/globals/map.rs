use crate::stdlib::MapConstructor;
use reflex::core::{Expression, ExpressionFactory};

pub fn global_map<T: Expression>(factory: &impl ExpressionFactory<T>) -> T
where
    T::Builtin: From<MapConstructor>,
{
    factory.create_builtin_term(MapConstructor)
}
