use crate::stdlib::ToString;
use reflex::core::{Expression, ExpressionFactory, HeapAllocator};

pub fn global_string<T: Expression>(
    factory: &impl ExpressionFactory<T>,
    _allocator: &impl HeapAllocator<T>,
) -> T
where
    T::Builtin: From<ToString>,
{
    factory.create_builtin_term(ToString)
}
