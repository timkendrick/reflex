use crate::stdlib::SetConstructor;
use reflex::core::{Expression, ExpressionFactory};

pub fn global_set<T: Expression>(factory: &impl ExpressionFactory<T>) -> T
where
    T::Builtin: From<SetConstructor>,
{
    factory.create_builtin_term(SetConstructor)
}
