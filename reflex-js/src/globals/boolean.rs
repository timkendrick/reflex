use reflex::core::{Expression, ExpressionFactory, HeapAllocator};
use reflex_stdlib::If;

pub fn global_boolean<T: Expression>(
    factory: &impl ExpressionFactory<T>,
    allocator: &impl HeapAllocator<T>,
) -> T
where
    T::Builtin: From<If>,
{
    factory.create_lambda_term(
        1,
        factory.create_application_term(
            factory.create_builtin_term(If),
            allocator.create_triple(
                factory.create_variable_term(0),
                factory.create_boolean_term(true),
                factory.create_boolean_term(false),
            ),
        ),
    )
}
