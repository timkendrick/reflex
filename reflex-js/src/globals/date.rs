use crate::stdlib::DateConstructor;
use reflex::core::{Expression, ExpressionFactory};

pub fn global_date<T: Expression>(factory: &impl ExpressionFactory<T>) -> T
where
    T::Builtin: From<DateConstructor>,
{
    factory.create_builtin_term(DateConstructor)
}
