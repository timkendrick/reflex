use reflex::core::{
    uuid, Applicable, ArgType, Arity, EvaluationCache, Expression, ExpressionFactory,
    FunctionArity, HeapAllocator, RecordTermType, Uid, Uuid,
};

pub struct StructTypeFactory;
impl StructTypeFactory {
    pub const UUID: Uuid = uuid!("69fdfb4f-7a4a-4414-8140-ededbdc9368c");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity() -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for StructTypeFactory {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
impl<T: Expression> Applicable<T> for StructTypeFactory {
    fn arity(&self) -> Option<Arity> {
        Some(Self::arity())
    }
    fn should_parallelize(&self, _args: &[T]) -> bool {
        false
    }
    fn apply(
        &self,
        args: impl ExactSizeIterator<Item = T>,
        factory: &impl ExpressionFactory<T>,
        allocator: &impl HeapAllocator<T>,
        _cache: &mut impl EvaluationCache<T>,
    ) -> Result<T, String> {
        let mut args = args.into_iter();
        let shape = args.next().unwrap();
        if let Some(shape) = factory.match_record_term(&shape) {
            Ok(
                factory
                    .create_constructor_term(allocator.clone_struct_prototype(shape.prototype())),
            )
        } else {
            Err(format!(
                "Invalid shape definition: Expected <struct>, received {}",
                shape
            ))
        }
    }
}
