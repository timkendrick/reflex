use std::time::SystemTime;

use chrono::{DateTime, Utc};

pub fn format_datetime(timestamp: SystemTime, format: &'static str) -> impl std::fmt::Display {
    let datetime: DateTime<Utc> = timestamp.into();
    datetime.format(format)
}
