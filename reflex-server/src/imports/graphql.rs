use crate::stdlib::GraphQlResolver;
use reflex::core::{create_record, Expression, ExpressionFactory, HeapAllocator};

pub fn import_graphql<T: Expression>(
    factory: &impl ExpressionFactory<T>,
    allocator: &impl HeapAllocator<T>,
) -> T
where
    T::Builtin: From<GraphQlResolver>,
{
    create_record(
        vec![(
            factory.create_string_term(allocator.create_static_string("Resolver")),
            factory.create_builtin_term(GraphQlResolver),
        )],
        factory,
        allocator,
    )
}
