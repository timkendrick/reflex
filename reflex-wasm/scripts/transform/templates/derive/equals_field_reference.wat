  (func (@concat "$" (@get $type_name) "::equals::" (@get $field_name)) (param $self i32) (param $other i32) (result i32)
    (call (@concat "$" (@get $target_type) "::traits::equals") (i32.load (local.get $self)) (i32.load (local.get $other))))
