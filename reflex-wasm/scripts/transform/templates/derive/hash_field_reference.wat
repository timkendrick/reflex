  (func (@concat "$" (@get $type_name) "::hash::" (@get $field_name)) (param $self i32) (param $state i64) (result i64)
    (call (@concat "$" (@get $target_type) "::traits::hash") (i32.load (local.get $self)) (local.get $state)))
