  (func (@concat "$" (@get $type_name) "::hash::" (@get $field_name)) (param $self i32) (param $state i64) (result i64)
    (call (@concat "$Hash::write_" (@get $field_type)) (local.get $state) (@instruction (@concat (@get $field_type) ".load") (local.get $self))))
