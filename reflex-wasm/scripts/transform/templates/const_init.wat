(@map $global
  (@get $globals)
  (@block
    (call (@list_item (@get $global) 2))
    (global.set (@list_item (@get $global) 0))))
