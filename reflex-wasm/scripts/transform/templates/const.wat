(@block
  (global (@get $identifier) (export (@concat "\"" (@get $identifier) "\"")) (mut (@get $type)) (@get $default))
  (@get $initializer))
