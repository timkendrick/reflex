import stdlib from './stdlib/index.test.mjs';
import termType from './term_type/index.test.mjs';

export default (describe) => {
  termType(describe);
  stdlib(describe);
};
