use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct CollectTree;
impl CollectTree {
    pub const UUID: Uuid = uuid!("3cd66a8c-5020-4f23-91f3-d294e54ab19f");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for CollectTree {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
