use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Identity;
impl Identity {
    pub const UUID: Uuid = uuid!("090b3058-4c8e-403a-98ff-ef471f87bf91");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Identity {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
