use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct IfPending;
impl IfPending {
    pub const UUID: Uuid = uuid!("ae41033f-ae13-4e46-810b-1a90d62aa306");
    const ARITY: FunctionArity<2, 0> = FunctionArity {
        required: [ArgType::Eager, ArgType::Lazy],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for IfPending {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
