use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct CollectHashset;
impl CollectHashset {
    pub const UUID: Uuid = uuid!("c264a811-ba79-4e1e-a3ef-78cb07fe0996");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for CollectHashset {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
