(module
  (@builtin $Stdlib_CollectString "CollectString"
    (@args (@strict $self))

    (@impl
      (call $TermType::implements::iterate)
      (func $Stdlib_CollectString::impl::<iterate> (param $self i32) (param $state i32) (result i32 i32)
        (call $Term::String::traits::collect_strict (local.get $self) (local.get $state))))

    (@default
      (func $Stdlib_CollectString::impl::default (param $self i32) (param $state i32) (result i32 i32)
        (call $Term::Signal::of
          (call $Term::Condition::invalid_builtin_function_args
            (global.get $Stdlib_CollectString)
            (call $Term::List::of (local.get $self))))
        (global.get $NULL)))))
