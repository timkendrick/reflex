export default (describe) => {
  describe('Stdlib_CollectHashset', (test) => {
    test('(Iterator)', (assert, {
      createApplication,
      createEmptyIterator,
      createFlattenIterator,
      createBuiltin,
      createInt,
      createOnceIterator,
      createRangeIterator,
      createString,
      createTriple,
      createUnitList,
      evaluate,
      format,
      hasHashsetValue,
      NULL,
      Stdlib,
    }) => {
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectHashset),
          createUnitList(createEmptyIterator()),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), 'Set(0)');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectHashset),
          createUnitList(
            createTriple(createString('foo'), createString('bar'), createString('baz')),
          ),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), 'Set(3)');
        assert.strictEqual(hasHashsetValue(result, createString('foo')), true);
        assert.strictEqual(hasHashsetValue(result, createString('bar')), true);
        assert.strictEqual(hasHashsetValue(result, createString('baz')), true);
        assert.strictEqual(hasHashsetValue(result, createString('qux')), false);
        assert.strictEqual(format(dependencies), 'NULL');
      })();
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectHashset),
          createUnitList(createRangeIterator(3, 3)),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), 'Set(3)');
        assert.strictEqual(hasHashsetValue(result, createInt(3)), true);
        assert.strictEqual(hasHashsetValue(result, createInt(4)), true);
        assert.strictEqual(hasHashsetValue(result, createInt(5)), true);
        assert.strictEqual(hasHashsetValue(result, createInt(6)), false);
        assert.strictEqual(format(dependencies), 'NULL');
      })();
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectHashset),
          createUnitList(createFlattenIterator(createOnceIterator(createRangeIterator(3, 3)))),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), 'Set(3)');
        assert.strictEqual(hasHashsetValue(result, createInt(3)), true);
        assert.strictEqual(hasHashsetValue(result, createInt(4)), true);
        assert.strictEqual(hasHashsetValue(result, createInt(5)), true);
        assert.strictEqual(hasHashsetValue(result, createInt(6)), false);
        assert.strictEqual(format(dependencies), 'NULL');
      })();
    });
  });
};
