use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Zip;
impl Zip {
    pub const UUID: Uuid = uuid!("fd604ce6-1dd0-460e-ba68-be3522f84168");
    const ARITY: FunctionArity<2, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Zip {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
