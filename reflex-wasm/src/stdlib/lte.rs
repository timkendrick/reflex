use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Lte;
impl Lte {
    pub const UUID: Uuid = uuid!("1ae64b30-9d16-4b54-adf2-3c457d49e546");
    const ARITY: FunctionArity<2, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Lte {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
