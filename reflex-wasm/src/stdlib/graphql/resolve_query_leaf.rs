use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct ResolveQueryLeaf;
impl ResolveQueryLeaf {
    pub const UUID: Uuid = uuid!("52299fbb-a84b-488e-81ef-98c439869f74");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for ResolveQueryLeaf {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
