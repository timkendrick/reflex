use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct ResolveQueryBranch;
impl ResolveQueryBranch {
    pub const UUID: Uuid = uuid!("58dd19b7-c9f0-473b-84c5-34af607c176b");
    const ARITY: FunctionArity<2, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for ResolveQueryBranch {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
