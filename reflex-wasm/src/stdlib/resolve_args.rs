use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct ResolveArgs;
impl ResolveArgs {
    pub const UUID: Uuid = uuid!("3d67b92a-e64e-419b-a4a9-8e49bd1eae92");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for ResolveArgs {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
