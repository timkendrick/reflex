export default (describe) => {
  describe('Stdlib_Effect', (test) => {
    test('(Symbol, List)', (assert, {
      createApplication,
      createBuiltin,
      createInt,
      createTriple,
      createSymbol,
      evaluate,
      format,
      NULL,
      Stdlib,
    }) => {
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.Effect),
          createTriple(createSymbol(123), createInt(3), createSymbol(456)),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '{<CustomCondition:Symbol(123):3:Symbol(456)>}');
        assert.strictEqual(
          format(dependencies),
          '(<CustomCondition:Symbol(123):3:Symbol(456)> . NULL)',
        );
      })();
    });
  });
};
