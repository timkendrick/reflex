use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Not;
impl Not {
    pub const UUID: Uuid = uuid!("f3a04848-e08d-4adb-a59d-19856b44ff9f");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Not {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
