use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Iterate;
impl Iterate {
    pub const UUID: Uuid = uuid!("36ecf042-6895-4e38-8a23-9e6208cbf9cf");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Iterate {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
