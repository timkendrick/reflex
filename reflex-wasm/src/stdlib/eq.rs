use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Eq;
impl Eq {
    pub const UUID: Uuid = uuid!("d6e1beca-6a8d-4297-a47f-084bda77b2c1");
    const ARITY: FunctionArity<2, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Eq {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
