use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Slice;
impl Slice {
    pub const UUID: Uuid = uuid!("03f6c061-0058-4ead-b72f-baf79eba31f1");
    const ARITY: FunctionArity<3, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Strict, ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Slice {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
