use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Flatten;
impl Flatten {
    pub const UUID: Uuid = uuid!("ea881ef8-0709-4ad2-9869-b1341af59e73");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Flatten {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
