(module
  (@builtin $Stdlib_Or "Or"
    (@args (@strict $self) (@lazy $other))

    (@default
      (func $Stdlib_Or::impl::default (param $self i32) (param $other i32) (param $state i32) (result i32 i32)
        (select
          (local.get $self)
          (local.get $other)
          (call $Term::traits::is_truthy (local.get $self)))
        (global.get $NULL)))))
