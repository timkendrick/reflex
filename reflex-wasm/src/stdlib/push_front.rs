use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct PushFront;
impl PushFront {
    pub const UUID: Uuid = uuid!("5029b060-3f1c-4df8-8a7f-6135590c5c1a");
    const ARITY: FunctionArity<2, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Lazy],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for PushFront {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
