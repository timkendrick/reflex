use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Divide;
impl Divide {
    pub const UUID: Uuid = uuid!("082fbcc9-31b0-4475-8e51-992d0ccc44dd");
    const ARITY: FunctionArity<2, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Divide {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
