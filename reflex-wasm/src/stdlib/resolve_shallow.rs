use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct ResolveShallow;
impl ResolveShallow {
    pub const UUID: Uuid = uuid!("475ca53b-e249-418d-8310-a9d54ae7ac0c");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for ResolveShallow {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
