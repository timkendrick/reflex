use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Car;
impl Car {
    pub const UUID: Uuid = uuid!("dd331a0f-3734-454c-a485-6565462220b4");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Car {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
