export default (describe) => {
  describe('Stdlib_CollectTree', (test) => {
    test('(Iterator)', (assert, {
      createApplication,
      createEmptyIterator,
      createFlattenIterator,
      createBuiltin,
      createOnceIterator,
      createRangeIterator,
      createUnitList,
      evaluate,
      format,
      NULL,
      Stdlib,
    }) => {
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectTree),
          createUnitList(createEmptyIterator()),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '(NULL . NULL)');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectTree),
          createUnitList(createRangeIterator(3, 3)),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '(5 . (4 . (3 . NULL)))');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectTree),
          createUnitList(createFlattenIterator(createOnceIterator(createRangeIterator(3, 3)))),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '(5 . (4 . (3 . NULL)))');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
    });
  });
};
