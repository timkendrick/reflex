use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Keys;
impl Keys {
    pub const UUID: Uuid = uuid!("05812cb5-1a05-49c0-be1a-cc715f620a77");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Keys {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
