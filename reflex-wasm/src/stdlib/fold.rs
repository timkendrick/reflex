use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Fold;
impl Fold {
    pub const UUID: Uuid = uuid!("f8312370-a299-457e-b9fb-f902f84f71b2");
    const ARITY: FunctionArity<3, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Strict, ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Fold {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
