use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Ceil;
impl Ceil {
    pub const UUID: Uuid = uuid!("1be66485-7ce2-40a4-8894-6df59a11661f");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Ceil {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
