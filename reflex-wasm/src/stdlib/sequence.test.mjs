export default (describe) => {
  describe('Stdlib_Sequence', (test) => {
    test('(Int, Builtin)', (assert, {
      createApplication,
      createBuiltin,
      createInt,
      createPair,
      evaluate,
      format,
      NULL,
      Stdlib,
    }) => {
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.Sequence),
          createPair(createInt(-3), createBuiltin(Stdlib.Abs)),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '3');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
    });

    test('(Application, Builtin)', (assert, {
      createApplication,
      createBuiltin,
      createInt,
      createPair,
      evaluate,
      format,
      NULL,
      Stdlib,
    }) => {
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.Sequence),
          createPair(
            createApplication(createBuiltin(Stdlib.Subtract), createPair(createInt(0), createInt(3))),
            createBuiltin(Stdlib.Abs),
          ),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '3');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
    });
  });
};
