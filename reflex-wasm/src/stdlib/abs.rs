use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Abs;
impl Abs {
    pub const UUID: Uuid = uuid!("493be078-9705-40d8-9967-b3d0867b0869");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Abs {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
