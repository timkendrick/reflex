(module
  (@builtin $Stdlib_CollectTree "CollectTree"
    (@args (@strict $self))

    (@impl
      (call $TermType::implements::iterate)
      (func $Stdlib_CollectTree::impl::<iterate> (param $self i32) (param $state i32) (result i32 i32)
        (call $Term::Tree::traits::collect_strict (local.get $self) (local.get $state))))

    (@default
      (func $Stdlib_CollectTree::impl::default (param $self i32) (param $state i32) (result i32 i32)
        (call $Term::Signal::of
          (call $Term::Condition::invalid_builtin_function_args
            (global.get $Stdlib_CollectTree)
            (call $Term::List::of (local.get $self))))
        (global.get $NULL)))))
