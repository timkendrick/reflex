use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct CollectString;
impl CollectString {
    pub const UUID: Uuid = uuid!("7b76c2ba-22db-4296-a317-f78c1a1bb6e3");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for CollectString {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
