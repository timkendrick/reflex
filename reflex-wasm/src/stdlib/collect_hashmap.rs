use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct CollectHashmap;
impl CollectHashmap {
    pub const UUID: Uuid = uuid!("72e23f09-c60c-48a8-9145-3170c37ca983");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for CollectHashmap {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
