use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct GraphQlResolver;
impl GraphQlResolver {
    pub const UUID: Uuid = uuid!("34f0156e-4a64-40f5-bd01-e297b9086ee7");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for GraphQlResolver {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
