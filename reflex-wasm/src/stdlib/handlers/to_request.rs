use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct ToRequest;
impl ToRequest {
    pub const UUID: Uuid = uuid!("29d89369-0b7b-41df-aa14-47ea708a8fa6");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for ToRequest {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
