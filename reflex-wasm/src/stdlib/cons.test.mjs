export default (describe) => {
  describe('Stdlib_Cons', (test) => {
    test('(Int, Int)', (assert, {
      createApplication,
      createBuiltin,
      createInt,
      createPair,
      evaluate,
      format,
      NULL,
      Stdlib,
    }) => {
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.Cons),
          createPair(createInt(3), createInt(4)),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '(3 . 4)');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
    });

    test('(Int, Nil)', (assert, {
      createApplication,
      createBuiltin,
      createInt,
      createNil,
      createPair,
      evaluate,
      format,
      NULL,
      Stdlib,
    }) => {
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.Cons),
          createPair(createInt(3), createNil()),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '(3 . null)');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
    });

    test('(Int, Tree)', (assert, {
      createApplication,
      createBuiltin,
      createTree,
      createInt,
      createPair,
      evaluate,
      format,
      NULL,
      Stdlib,
    }) => {
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.Cons),
          createPair(createInt(3), createTree(createInt(4), NULL)),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '(3 . (4 . NULL))');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.Cons),
          createPair(createInt(3), createTree(createInt(4), createInt(5))),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '(3 . (4 . 5))');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
    });
  });
};
