use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Sequence;
impl Sequence {
    pub const UUID: Uuid = uuid!("c19bb80d-f021-4f0d-91af-6584d48a45a6");
    const ARITY: FunctionArity<2, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Sequence {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
