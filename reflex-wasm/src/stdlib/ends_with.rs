use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct EndsWith;
impl EndsWith {
    pub const UUID: Uuid = uuid!("dba85cb9-fefd-433f-86a4-abe762a880f9");
    const ARITY: FunctionArity<2, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for EndsWith {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
