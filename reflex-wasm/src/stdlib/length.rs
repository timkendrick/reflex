use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Length;
impl Length {
    pub const UUID: Uuid = uuid!("e1f49f93-3125-48d4-8686-8c09f33c1a07");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Length {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
