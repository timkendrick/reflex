use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct ParseJson;
impl ParseJson {
    pub const UUID: Uuid = uuid!("6e8468d3-34cc-423c-a787-3d3e9154885d");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for ParseJson {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
