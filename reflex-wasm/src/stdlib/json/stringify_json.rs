use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct StringifyJson;
impl StringifyJson {
    pub const UUID: Uuid = uuid!("b0a2caca-1101-402c-a21e-bccdf276e44c");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for StringifyJson {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
