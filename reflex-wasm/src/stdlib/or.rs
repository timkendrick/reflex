use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Or;
impl Or {
    pub const UUID: Uuid = uuid!("b4798927-f64e-4835-962e-e3ff1fbe3153");
    const ARITY: FunctionArity<2, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Lazy],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Or {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
