use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct IsFinite;
impl IsFinite {
    pub const UUID: Uuid = uuid!("fc23d93b-fb6b-412b-a3ca-ad2d30953aeb");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for IsFinite {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
