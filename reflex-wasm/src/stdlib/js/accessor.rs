use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Accessor;
impl Accessor {
    pub const UUID: Uuid = uuid!("c6ea1ed6-9361-4ba7-813e-1bbe09331f8d");
    const ARITY: FunctionArity<2, 0> = FunctionArity {
        required: [ArgType::Strict, ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Accessor {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
