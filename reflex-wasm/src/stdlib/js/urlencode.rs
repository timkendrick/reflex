use reflex::core::{uuid, ArgType, Arity, FunctionArity, Uid, Uuid};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Urlencode;
impl Urlencode {
    pub const UUID: Uuid = uuid!("ad730068-31a7-49a4-ae09-ecbda0c9914a");
    const ARITY: FunctionArity<1, 0> = FunctionArity {
        required: [ArgType::Strict],
        optional: [],
        variadic: None,
    };
    pub fn arity(&self) -> Arity {
        Arity::from(&Self::ARITY)
    }
}
impl Uid for Urlencode {
    fn uid(&self) -> Uuid {
        Self::UUID
    }
}
