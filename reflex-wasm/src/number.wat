(module
  ;; Imported floating point functions
  (func $Utils::Float::to_string (import "Number" "toString") (param f64 i32) (result i32)))
