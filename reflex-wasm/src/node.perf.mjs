import path from 'path';
import url from 'url';

import createWasmTestRunner from './node.runner.mjs';

import tests from './perf.test.mjs';

const __dirname = path.dirname(url.fileURLToPath(import.meta.url));

const runner = createWasmTestRunner(path.join(__dirname, `../build/runtime.release.wasm`));

runner(tests).then((success) => {
  process.exit(success ? 0 : 1);
});
