use std::{collections::HashSet, iter::once};

use reflex::{
    core::{
        DependencyList, Eagerness, Expression, ExpressionListType, GraphNode, Internable,
        ListTermType, SerializeJson, StackOffset, StructPrototypeType,
    },
    hash::HashId,
};
use reflex_utils::{json::is_empty_json_object, MapIntoIterator};
use serde_json::Value as JsonValue;

use crate::{
    allocator::{Arena, ArenaAllocator},
    hash::{TermHash, TermHasher, TermSize},
    term_type::TermType,
    term_type::TypedTerm,
    ArenaArrayIter, ArenaPointer, ArenaRef, Array, IntoArenaRefIter, PointerIter, Term,
};

use super::WasmExpression;

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct ListTerm {
    pub items: Array<ArenaPointer>,
}

pub type ListTermPointerIter = std::vec::IntoIter<ArenaPointer>;

impl<A: Arena> PointerIter for ArenaRef<ListTerm, A> {
    type Iter<'a> = ListTermPointerIter
    where
        Self: 'a;

    fn iter<'a>(&self) -> Self::Iter<'a>
    where
        Self: 'a,
    {
        self.read_value(|term| {
            let ptr = term as *const ListTerm as u32;

            term.items
                .items()
                .map(|item| {
                    self.pointer
                        .offset(item as *const ArenaPointer as u32 - ptr)
                })
                .collect::<Vec<_>>()
        })
        .into_iter()
    }
}

impl TermSize for ListTerm {
    fn size_of(&self) -> usize {
        std::mem::size_of::<Self>() - std::mem::size_of::<Array<ArenaPointer>>()
            + self.items.size_of()
    }
}
impl TermHash for ListTerm {
    fn hash(&self, hasher: TermHasher, arena: &impl Arena) -> TermHasher {
        hasher.hash(&self.items, arena)
    }
}
impl ListTerm {
    pub fn allocate(
        values: impl IntoIterator<
            Item = ArenaPointer,
            IntoIter = impl ExactSizeIterator<Item = ArenaPointer>,
        >,
        arena: &mut impl ArenaAllocator,
    ) -> ArenaPointer {
        let values = values.into_iter();
        let term = Term::new(
            TermType::List(ListTerm {
                items: Default::default(),
            }),
            arena,
        );
        let term_size = term.size_of();
        let instance = arena.allocate(term);
        let list = instance.offset((term_size - std::mem::size_of::<Array<ArenaPointer>>()) as u32);
        Array::<ArenaPointer>::extend(list, values, arena);
        let hash = arena.read_value::<Term, _>(instance, |term| {
            TermHasher::default().hash(term, arena).finish()
        });
        arena.write::<u64>(Term::get_hash_pointer(instance), u64::from(hash));
        instance
    }
}

impl<A: Arena + Clone> ArenaRef<ListTerm, A> {
    fn items_pointer(&self) -> ArenaPointer {
        self.inner_pointer(|value| &value.items)
    }
    pub fn items(&self) -> ArenaRef<Array<ArenaPointer>, A> {
        ArenaRef::<Array<ArenaPointer>, _>::new(self.arena.clone(), self.items_pointer())
    }
    pub fn iter(&self) -> IntoArenaRefIter<'_, Term, A, ArenaArrayIter<'_, ArenaPointer, A>> {
        IntoArenaRefIter::new(
            &self.arena,
            Array::<ArenaPointer>::iter(self.items_pointer(), &self.arena),
        )
    }
    pub fn len(&self) -> usize {
        self.items().len()
    }
}

impl<A: Arena + Clone> GraphNode for ArenaRef<ListTerm, A> {
    fn size(&self) -> usize {
        1 + self.iter().map(|term| term.size()).sum::<usize>()
    }
    fn capture_depth(&self) -> StackOffset {
        self.iter()
            .map(|term| term.capture_depth())
            .max()
            .unwrap_or(0)
    }
    fn free_variables(&self) -> HashSet<StackOffset> {
        self.iter().flat_map(|term| term.free_variables()).collect()
    }
    fn count_variable_usages(&self, offset: StackOffset) -> usize {
        self.iter()
            .map(|term| term.count_variable_usages(offset))
            .sum()
    }
    fn dynamic_dependencies(&self, deep: bool) -> DependencyList {
        if deep {
            self.iter()
                .flat_map(|term| term.dynamic_dependencies(deep))
                .collect()
        } else {
            DependencyList::empty()
        }
    }
    fn has_dynamic_dependencies(&self, deep: bool) -> bool {
        if deep {
            self.iter().any(|term| term.has_dynamic_dependencies(deep))
        } else {
            false
        }
    }
    fn is_static(&self) -> bool {
        true
    }
    fn is_atomic(&self) -> bool {
        self.iter().all(|term| term.is_atomic())
    }
    fn is_complex(&self) -> bool {
        true
    }
}

impl<A: Arena + Clone> ListTermType<WasmExpression<A>> for ArenaRef<TypedTerm<ListTerm>, A> {
    fn items<'a>(&'a self) -> <WasmExpression<A> as Expression>::ExpressionListRef<'a>
    where
        <WasmExpression<A> as Expression>::ExpressionList: 'a,
        WasmExpression<A>: 'a,
    {
        self.clone()
    }
}

impl<A: Arena + Clone> StructPrototypeType<WasmExpression<A>> for ArenaRef<TypedTerm<ListTerm>, A> {
    fn keys<'a>(&'a self) -> <WasmExpression<A> as Expression>::ExpressionListRef<'a>
    where
        <WasmExpression<A> as Expression>::ExpressionList: 'a,
        WasmExpression<A>: 'a,
    {
        self.clone()
    }
}

impl<A: Arena + Clone> ExpressionListType<WasmExpression<A>> for ArenaRef<TypedTerm<ListTerm>, A> {
    type Iterator<'a> = MapIntoIterator<
        IntoArenaRefIter<'a, Term, A, ArenaArrayIter<'a, ArenaPointer, A>>,
        ArenaRef<Term, A>,
        <WasmExpression<A> as Expression>::ExpressionRef<'a>,
    >
    where
        WasmExpression<A>: 'a,
        Self: 'a;
    fn id(&self) -> HashId {
        self.read_value(|term| term.id())
    }
    fn len(&self) -> usize {
        self.as_inner().items().len()
    }
    fn get<'a>(
        &'a self,
        index: usize,
    ) -> Option<<WasmExpression<A> as Expression>::ExpressionRef<'a>>
    where
        WasmExpression<A>: 'a,
    {
        self.as_inner()
            .items()
            .get(index)
            .map(|pointer| ArenaRef::<Term, _>::new(self.arena.clone(), pointer).into())
    }
    fn iter<'a>(&'a self) -> Self::Iterator<'a>
    where
        WasmExpression<A>: 'a,
    {
        MapIntoIterator::new(IntoArenaRefIter::new(
            &self.arena,
            Array::<ArenaPointer>::iter(self.as_inner().items_pointer(), &self.arena),
        ))
    }
}

impl<A: Arena + Clone> SerializeJson for ArenaRef<ListTerm, A> {
    fn to_json(&self) -> Result<JsonValue, String> {
        self.iter()
            .map(|key| key.to_json())
            .collect::<Result<Vec<_>, String>>()
            .map(|values| JsonValue::Array(values))
    }
    fn patch(&self, target: &Self) -> Result<Option<JsonValue>, String> {
        let updates = target
            .iter()
            .zip(self.iter())
            .map(|(current, previous)| previous.patch(&current))
            .chain(
                target
                    .iter()
                    .skip(self.len())
                    .map(|item| item.to_json().map(Some)),
            )
            .collect::<Result<Vec<_>, _>>()?;
        let updates = reflex_utils::json::json_object(
            updates
                .into_iter()
                .enumerate()
                .filter_map(|(index, item)| item.map(|value| (index.to_string(), value)))
                .chain(if target.len() != self.len() {
                    Some((String::from("length"), JsonValue::from(target.len())))
                } else {
                    None
                }),
        );
        if is_empty_json_object(&updates) {
            Ok(None)
        } else {
            Ok(Some(updates))
        }
    }
}

impl<A: Arena + Clone> PartialEq for ArenaRef<ListTerm, A> {
    fn eq(&self, other: &Self) -> bool {
        // TODO: Clarify PartialEq implementations for container terms
        // This assumes that lists with the same length and hash are almost certainly identical
        self.len() == other.len()
    }
}
impl<A: Arena + Clone> Eq for ArenaRef<ListTerm, A> {}

impl<A: Arena + Clone> std::fmt::Debug for ArenaRef<ListTerm, A> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.read_value(|term| std::fmt::Debug::fmt(term, f))
    }
}

impl<A: Arena + Clone> std::fmt::Display for ArenaRef<ListTerm, A> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let max_displayed_items = 100;
        let items = self.iter();
        let num_items = items.len();
        write!(
            f,
            "[{}]",
            if num_items <= max_displayed_items {
                items
                    .map(|item| format!("{}", item))
                    .collect::<Vec<_>>()
                    .join(", ")
            } else {
                items
                    .take(max_displayed_items - 1)
                    .map(|item| format!("{}", item))
                    .chain(once(format!(
                        "...{} more items",
                        num_items - (max_displayed_items - 1)
                    )))
                    .collect::<Vec<_>>()
                    .join(", ")
            }
        )
    }
}

impl<A: Arena + Clone> Internable for ArenaRef<ListTerm, A> {
    fn should_intern(&self, _eager: Eagerness) -> bool {
        self.capture_depth() == 0
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        allocator::VecAllocator,
        term_type::{IntTerm, TermType, TermTypeDiscriminants},
        utils::chunks_to_u64,
    };

    use super::*;

    #[test]
    fn list() {
        assert_eq!(
            TermType::List(ListTerm {
                items: Default::default()
            })
            .as_bytes(),
            [TermTypeDiscriminants::List as u32, 0, 0],
        );
        let mut allocator = VecAllocator::default();
        {
            let first_item =
                allocator.allocate(Term::new(TermType::Int(IntTerm::from(3)), &allocator));
            let second_item =
                allocator.allocate(Term::new(TermType::Int(IntTerm::from(4)), &allocator));
            let entries = [first_item, second_item];
            let instance = ListTerm::allocate(entries, &mut allocator);
            let result = allocator.get_ref::<Term>(instance).as_bytes();
            // TODO: Test term hashing
            let _hash = chunks_to_u64([result[0], result[1]]);
            let discriminant = result[2];
            let data_length = result[3];
            let data_capacity = result[4];
            let data = &result[5..];
            assert_eq!(discriminant, TermTypeDiscriminants::List as u32);
            assert_eq!(data_length, entries.len() as u32);
            assert_eq!(data_capacity, entries.len() as u32);
            assert_eq!(data, [u32::from(first_item), u32::from(second_item)]);
        }
    }
}
