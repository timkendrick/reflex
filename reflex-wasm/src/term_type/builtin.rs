use std::collections::HashSet;

use reflex::core::{
    Arity, BuiltinTermType, DependencyList, Eagerness, Expression, GraphNode, Internable,
    SerializeJson, StackOffset,
};
use serde_json::Value as JsonValue;

use crate::{
    allocator::Arena,
    hash::{TermHash, TermHasher, TermSize},
    stdlib::Stdlib,
    term_type::TypedTerm,
    ArenaRef,
};
use reflex_macros::PointerIter;

#[derive(PartialEq, Eq, Clone, Copy, Debug, PointerIter)]
#[repr(C)]
pub struct FunctionIndex(u32);
impl TermSize for FunctionIndex {
    fn size_of(&self) -> usize {
        std::mem::size_of::<Self>()
    }
}
impl TermHash for FunctionIndex {
    fn hash(&self, hasher: TermHasher, arena: &impl Arena) -> TermHasher {
        let Self(uid) = self;
        hasher.hash(uid, arena)
    }
}
impl From<FunctionIndex> for u32 {
    fn from(value: FunctionIndex) -> Self {
        let FunctionIndex(value) = value;
        value
    }
}
impl From<Stdlib> for FunctionIndex {
    fn from(value: Stdlib) -> Self {
        Self(u32::from(value))
    }
}
impl std::fmt::Display for FunctionIndex {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match Stdlib::try_from(self.0) {
            Ok(stdlib) => write!(f, "{}", stdlib),
            Err(_) => write!(f, "<unknown:{}>", self.0),
        }
    }
}

#[derive(Clone, Copy, Debug, PointerIter)]
#[repr(C)]
pub struct BuiltinTerm {
    pub uid: FunctionIndex,
}
impl TermSize for BuiltinTerm {
    fn size_of(&self) -> usize {
        std::mem::size_of::<Self>()
    }
}
impl TermHash for BuiltinTerm {
    fn hash(&self, hasher: TermHasher, arena: &impl Arena) -> TermHasher {
        hasher.hash(&self.uid, arena)
    }
}
impl From<Stdlib> for BuiltinTerm {
    fn from(value: Stdlib) -> Self {
        Self { uid: value.into() }
    }
}
impl std::fmt::Display for BuiltinTerm {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match Stdlib::try_from(u32::from(self.uid)) {
            Ok(builtin) => std::fmt::Display::fmt(&builtin, f),
            Err(_) => std::fmt::Debug::fmt(self, f),
        }
    }
}

impl<A: Arena + Clone> ArenaRef<BuiltinTerm, A> {
    pub fn target(&self) -> FunctionIndex {
        self.read_value(|term| term.uid)
    }
    pub fn arity(&self) -> Option<Arity> {
        Stdlib::try_from(u32::from(self.target()))
            .ok()
            .map(|builtin| builtin.arity())
    }
}

impl<T: Expression, A: Arena + Clone> BuiltinTermType<T> for ArenaRef<BuiltinTerm, A>
where
    T::Builtin: From<FunctionIndex>,
{
    fn target<'a>(&'a self) -> T::Builtin
    where
        T: 'a,
        T::Builtin: 'a,
    {
        T::Builtin::from(self.target())
    }
}

impl<T: Expression, A: Arena + Clone> BuiltinTermType<T> for ArenaRef<TypedTerm<BuiltinTerm>, A>
where
    T::Builtin: From<FunctionIndex>,
{
    fn target<'a>(&'a self) -> T::Builtin
    where
        T: 'a,
        T::Builtin: 'a,
    {
        <ArenaRef<BuiltinTerm, A> as BuiltinTermType<T>>::target(&self.as_inner())
    }
}

impl<A: Arena + Clone> GraphNode for ArenaRef<BuiltinTerm, A> {
    fn size(&self) -> usize {
        1
    }
    fn capture_depth(&self) -> StackOffset {
        0
    }
    fn free_variables(&self) -> HashSet<StackOffset> {
        HashSet::new()
    }
    fn count_variable_usages(&self, _offset: StackOffset) -> usize {
        0
    }
    fn dynamic_dependencies(&self, _deep: bool) -> DependencyList {
        DependencyList::empty()
    }
    fn has_dynamic_dependencies(&self, _deep: bool) -> bool {
        false
    }
    fn is_static(&self) -> bool {
        true
    }
    fn is_atomic(&self) -> bool {
        true
    }
    fn is_complex(&self) -> bool {
        false
    }
}

impl<A: Arena + Clone> SerializeJson for ArenaRef<BuiltinTerm, A> {
    fn to_json(&self) -> Result<JsonValue, String> {
        Err(format!("Unable to serialize term: {}", self))
    }
    fn patch(&self, target: &Self) -> Result<Option<JsonValue>, String> {
        Err(format!(
            "Unable to create patch for terms: {}, {}",
            self, target
        ))
    }
}

impl<A: Arena + Clone> PartialEq for ArenaRef<BuiltinTerm, A> {
    fn eq(&self, other: &Self) -> bool {
        self.target() == other.target()
    }
}
impl<A: Arena + Clone> Eq for ArenaRef<BuiltinTerm, A> {}

impl<A: Arena + Clone> std::fmt::Debug for ArenaRef<BuiltinTerm, A> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.read_value(|term| std::fmt::Debug::fmt(term, f))
    }
}

impl<A: Arena + Clone> std::fmt::Display for ArenaRef<BuiltinTerm, A> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<stdlib:{}>", self.target())
    }
}

impl<A: Arena + Clone> Internable for ArenaRef<BuiltinTerm, A> {
    fn should_intern(&self, _eager: Eagerness) -> bool {
        self.capture_depth() == 0
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        stdlib::{Add, StdlibDiscriminants},
        term_type::{TermType, TermTypeDiscriminants},
    };

    use super::*;

    #[test]
    fn builtin() {
        assert_eq!(
            TermType::Builtin(BuiltinTerm::from(Stdlib::from(Add))).as_bytes(),
            [
                TermTypeDiscriminants::Builtin as u32,
                StdlibDiscriminants::Add as u32
            ],
        );
    }
}
