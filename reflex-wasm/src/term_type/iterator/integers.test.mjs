export default (describe) => {
  describe('Term::IntegersIterator', (test) => {
    test('iteration', (assert, {
      createIntegersIterator,
      createBuiltin,
      createTakeIterator,
      createApplication,
      createUnitList,
      evaluate,
      format,
      NULL,
      Stdlib,
    }) => {
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectList),
          createUnitList(createTakeIterator(createIntegersIterator(), 0)),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '[]');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectList),
          createUnitList(createTakeIterator(createIntegersIterator(), 5)),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '[0, 1, 2, 3, 4]');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
    });
  });
};
