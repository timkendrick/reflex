use std::collections::HashSet;

use reflex::core::{DependencyList, Eagerness, GraphNode, Internable, SerializeJson, StackOffset};
use serde_json::Value as JsonValue;

use crate::{
    allocator::Arena,
    hash::{TermHash, TermHasher, TermSize},
    ArenaPointer, ArenaRef, Term,
};
use reflex_macros::PointerIter;

#[derive(Clone, Copy, Debug, PointerIter)]
#[repr(C)]
pub struct ZipIteratorTerm {
    pub left: ArenaPointer,
    pub right: ArenaPointer,
}
impl TermSize for ZipIteratorTerm {
    fn size_of(&self) -> usize {
        std::mem::size_of::<Self>()
    }
}
impl TermHash for ZipIteratorTerm {
    fn hash(&self, hasher: TermHasher, arena: &impl Arena) -> TermHasher {
        hasher.hash(&self.left, arena).hash(&self.right, arena)
    }
}

impl<A: Arena + Clone> ArenaRef<ZipIteratorTerm, A> {
    pub fn left(&self) -> ArenaRef<Term, A> {
        ArenaRef::<Term, _>::new(self.arena.clone(), self.read_value(|term| term.left))
    }
    pub fn right(&self) -> ArenaRef<Term, A> {
        ArenaRef::<Term, _>::new(self.arena.clone(), self.read_value(|term| term.right))
    }
}

impl<A: Arena + Clone> SerializeJson for ArenaRef<ZipIteratorTerm, A> {
    fn to_json(&self) -> Result<JsonValue, String> {
        Err(format!("Unable to serialize term: {}", self))
    }
    fn patch(&self, target: &Self) -> Result<Option<JsonValue>, String> {
        Err(format!(
            "Unable to create patch for terms: {}, {}",
            self, target
        ))
    }
}

impl<A: Arena + Clone> PartialEq for ArenaRef<ZipIteratorTerm, A> {
    fn eq(&self, other: &Self) -> bool {
        self.left() == other.left() && self.right() == other.right()
    }
}
impl<A: Arena + Clone> Eq for ArenaRef<ZipIteratorTerm, A> {}

impl<A: Arena + Clone> std::fmt::Debug for ArenaRef<ZipIteratorTerm, A> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.read_value(|term| std::fmt::Debug::fmt(term, f))
    }
}

impl<A: Arena + Clone> std::fmt::Display for ArenaRef<ZipIteratorTerm, A> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "ZipIterator")
    }
}

impl<A: Arena + Clone> GraphNode for ArenaRef<ZipIteratorTerm, A> {
    fn size(&self) -> usize {
        1 + self.left().size() + self.right().size()
    }
    fn capture_depth(&self) -> StackOffset {
        self.left()
            .capture_depth()
            .max(self.right().capture_depth())
    }
    fn free_variables(&self) -> HashSet<StackOffset> {
        self.left().free_variables()
    }
    fn count_variable_usages(&self, offset: StackOffset) -> usize {
        self.left().count_variable_usages(offset)
    }
    fn dynamic_dependencies(&self, deep: bool) -> DependencyList {
        if deep {
            self.left()
                .dynamic_dependencies(deep)
                .into_iter()
                .chain(self.right().dynamic_dependencies(deep))
                .collect()
        } else {
            DependencyList::empty()
        }
    }
    fn has_dynamic_dependencies(&self, deep: bool) -> bool {
        if deep {
            self.left().has_dynamic_dependencies(deep)
                || self.right().has_dynamic_dependencies(deep)
        } else {
            false
        }
    }
    fn is_static(&self) -> bool {
        true
    }
    fn is_atomic(&self) -> bool {
        self.left().is_atomic() && self.right().is_atomic()
    }
    fn is_complex(&self) -> bool {
        true
    }
}

impl<A: Arena + Clone> Internable for ArenaRef<ZipIteratorTerm, A> {
    fn should_intern(&self, _eager: Eagerness) -> bool {
        self.capture_depth() == 0
    }
}

#[cfg(test)]
mod tests {
    use crate::term_type::{TermType, TermTypeDiscriminants};

    use super::*;

    #[test]
    fn zip_iterator() {
        assert_eq!(
            TermType::ZipIterator(ZipIteratorTerm {
                left: ArenaPointer(0x54321),
                right: ArenaPointer(0x98765),
            })
            .as_bytes(),
            [TermTypeDiscriminants::ZipIterator as u32, 0x54321, 0x98765],
        );
    }
}
