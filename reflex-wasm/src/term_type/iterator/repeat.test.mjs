export default (describe) => {
  describe('Term::RepeatIterator', (test) => {
    test('iteration', (assert, {
      createApplication,
      createRepeatIterator,
      createBuiltin,
      createInt,
      createTakeIterator,
      createUnitList,
      evaluate,
      format,
      NULL,
      Stdlib,
    }) => {
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectList),
          createUnitList(createTakeIterator(createRepeatIterator(createInt(3)), 0)),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '[]');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectList),
          createUnitList(createTakeIterator(createRepeatIterator(createInt(3)), 5)),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '[3, 3, 3, 3, 3]');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
    });
  });
};
