export default (describe) => {
  describe('Term::HashmapKeysIterator', (test) => {
    test('iteration', (assert, {
      createApplication,
      createBuiltin,
      createInt,
      createHashmap,
      createHashmapKeysIterator,
      createString,
      createUnitList,
      evaluate,
      format,
      getListItems,
      isList,
      NULL,
      Stdlib,
    }) => {
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectList),
          createUnitList(createHashmapKeysIterator(createHashmap([]))),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.strictEqual(format(result), '[]');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
      (() => {
        const expression = createApplication(
          createBuiltin(Stdlib.CollectList),
          createUnitList(
            createHashmapKeysIterator(
              createHashmap([
                [createString('foo'), createInt(3)],
                [createString('bar'), createInt(4)],
                [createString('baz'), createInt(5)],
              ]),
            ),
          ),
        );
        const [result, dependencies] = evaluate(expression, NULL);
        assert.ok(isList(result));
        assert.strictEqual(`[${getListItems(result).map(format).sort().join(', ')}]`, '["bar", "baz", "foo"]');
        assert.strictEqual(format(dependencies), 'NULL');
      })();
    });
  });
};
