(module
  ;; Alias for boolean i32 values
  (global $FALSE i32 (i32.const 0))
  (global $TRUE i32 (i32.const 1))
  ;; Sentinel value used to indicate missing values
  (global $NULL (export "NULL") i32 (i32.const 0xFFFFFFFF)))
