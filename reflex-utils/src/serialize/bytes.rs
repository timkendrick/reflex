use bytes::Bytes;

pub fn serialize<'a, S>(value: &'a Bytes, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    serializer.serialize_bytes(&value)
}

pub fn deserialize<'de, D>(deserializer: D) -> Result<Bytes, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let bytes: Box<[u8]> = serde::Deserialize::deserialize(deserializer)?;
    Ok(bytes.into())
}
